from fastapi import FastAPI
from api.queries import router as queries_router

app = FastAPI()

app.include_router(queries_router, prefix="/queries", tags=["queries"])