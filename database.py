import sqlite3

class Database:
    def __init__(self, db_name):
        self.conn = sqlite3.connect(db_name)
        self.create_table()

    def create_table(self):
        query = """
        CREATE TABLE IF NOT EXISTS queries (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            query TEXT NOT NULL
        );
        """
        self.conn.execute(query)
        self.conn.commit()

    def insert_query(self, query):
        insert_query = """
        INSERT INTO queries (query) VALUES (?);
        """
        self.conn.execute(insert_query, (query,))
        self.conn.commit()

    def get_queries(self):
        select_query = """
        SELECT * FROM queries;
        """
        cursor = self.conn.execute(select_query)
        queries = []
        for row in cursor:
            query = {
                "id": row[0],
                "query": row[1]
            }
            queries.append(query)
        return queries

    def get_query(self, query_id):
        select_query = """
        SELECT * FROM queries WHERE id = ?;
        """
        cursor = self.conn.execute(select_query, (query_id,))
        row = cursor.fetchone()
        if row:
            query = {
                "id": row[0],
                "query": row[1]
            }
            return query
        return None

    def update_query(self, query_id, new_query):
        update_query = """
        UPDATE queries SET query = ? WHERE id = ?;
        """
        self.conn.execute(update_query, (new_query, query_id))
        self.conn.commit()

    def delete_query(self, query_id):
        delete_query = """
        DELETE FROM queries WHERE id = ?;
        """
        self.conn.execute(delete_query, (query_id,))
        self.conn.commit()

    def close_connection(self):
        self.conn.close()