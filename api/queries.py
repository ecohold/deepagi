from fastapi import APIRouter
from models import Query
from bert import BertModel
from database import Database

router = APIRouter()
bert_model = BertModel()
db = Database("deepbert.db")

@router.get("/answer")
def get_query_answer(query: str, context: str):
    answer = bert_model.answer_question(context, query)
    return {"query": query, "context": context, "answer": answer}

@router.get("/queries")
def get_queries():
    queries = db.get_queries()
    return queries

@router.get("/queries/{query_id}")
def get_query(query_id: int):
    query = db.get_query(query_id)
    if query:
        return query
    return {"message": "Query not found"}

@router.post("/queries")
def create_query(query: Query):
    db.insert_query(query.query)
    return {"message": "Query created successfully"}

@router.put("/queries/{query_id}")
def update_query(query_id: int, query: Query):
    db.update_query(query_id, query.query)
    return {"message": "Query updated successfully"}

@router.delete("/queries/{query_id}")
def delete_query(query_id: int):
    db.delete_query(query_id)
    return {"message": "Query deleted successfully"}