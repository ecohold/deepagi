from transformers import BertForQuestionAnswering, BertTokenizer

class BertModel:
    def __init__(self):
        self.model = BertForQuestionAnswering.from_pretrained("bert-base-uncased")
        self.tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

    def answer_question(self, context, question):
        encoding = self.tokenizer.encode_plus(question, context, return_tensors="pt")
        input_ids = encoding["input_ids"]
        attention_mask = encoding["attention_mask"]

        start_scores, end_scores = self.model(input_ids, attention_mask=attention_mask)

        all_tokens = self.tokenizer.convert_ids_to_tokens(input_ids[0])
        answer = ' '.join(all_tokens[torch.argmax(start_scores) : torch.argmax(end_scores) + 1])
        answer = self.tokenizer.convert_tokens_to_string(answer)
        return answer