from pydantic import BaseModel

class Query(BaseModel):
    id: int
    query: str